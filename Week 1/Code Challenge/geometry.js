const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function prism(La, t) {
  console.log("Prism's Volume = " + La * t);
}

console.log("\nPrims's Volume Calculation = La x t\n");

rl.question("Enter value of La! ", (La) => {
  rl.question("Enter value of t! ", (t) => {
    prism(La, t);

    console.log("--------------------------------------------------------");
    function pyramid(La, t) {
      console.log("Rectangular Pyramid's Volume = " + (1 / 3) * La * t);
    }

    console.log("\nRectangular Pyramid's Volume Calculation = 1/3 * La * t\n");

    rl.question("Enter value of La! ", (La) => {
      rl.question("Enter value of t! ", (t) => {
        pyramid(La, t);

        rl.close();
      });
    });

    rl.on("close", () => {
      process.exit();
    });
  });
});
