// Importing Module
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Code here!
const currentDate = new Date();
const currentYear = currentDate.getFullYear();

function greet(name, address, birthday) {
  const age = currentYear - birthday;
  console.log(
    `\nHello ${name}, looks like you're ${age}! And you live in ${address}`
  );
}

// DON'T CHANGE
console.log("\nGoverment Registry\n");
// GET User's Name
rl.question("What is your name? ", (name) => {
  console.log(`Well, your name is ${name}`);
  // GET User's Address
  rl.question("\nWhich city do you live? ", (address) => {
    console.log(`Then, you live in ${address} right now`);
    // GET User's Birthday
    rl.question("\nWhen was your birthday year? ", (birthday) => {
      console.log(`And ${birthday} is your birthday year`);
      greet(name, address, birthday);

      rl.close();
    });
  });
});

rl.on("close", () => {
  process.exit();
});
