const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const qLoop = function () {
  rl.question(
    "\nPlease Choose an option:\n" +
      "1) Prism Volume\n" +
      "2) Rectangular Pyramid's Volume\n" +
      "3) Exit\n",

    function (line) {
      switch (line) {
        case "1":
          function prism(baseArea, height) {
            return baseArea * height;
          }

          // Function for inputing Base Area of Prism
          function inputBaseAreaPrism() {
            rl.question(`Base Area: `, (baseArea) => {
              if (!isNaN(baseArea)) {
                inputHeightPrism(baseArea);
              } else {
                console.log(`Base Area must be a number\n`);
                inputBaseAreaPrism();
              }
            });
          }

          // Function for inputing Height of Prism
          function inputHeightPrism(baseArea) {
            rl.question(`Height: `, (height) => {
              if (!isNaN(height)) {
                console.log(`\nPrism: ${prism(baseArea, height)}`);
                qLoop();
              } else {
                console.log(`Height must be a number\n`);
                inputHeightPrism(baseArea);
              }
            });
          }

          console.log(`Prism's Volume`);
          console.log(`==============`);
          inputBaseAreaPrism();
          break;

        case "2":
          function pyramid(B, h) {
            return (1 / 3) * B * h;
          }

          function inputBaseAreaPyramid() {
            rl.question("Base Area: ", (B) => {
              rl.question("Height: ", (h) => {
                if (!isNaN(B) && !isNaN(h)) {
                  console.log(`\nPyramid: ${pyramid(B, h)}`);
                  qLoop();
                } else {
                  console.log(`Base Area and Height must be a number\n`);
                  inputBaseAreaPyramid();
                }
              });
            });
          }

          console.log(`Pyramid's Volume`);
          console.log(`==============`);
          inputBaseAreaPyramid();
          break;

        case "3":
          return rl.close();
        default:
          console.log("No such option. Please enter another: ");
      }
      qLoop();
    }
  );
};
qLoop();
