const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const covid = [
  {
    name: "Pandji",
    status: "Positive",
  },
  {
    name: "Bayu",
    status: "Positive",
  },
  {
    name: "Satria",
    status: "Negative",
  },
  {
    name: "Danu",
    status: "Negative",
  },
  {
    name: "Bimo",
    status: "Suspect",
  },
];

function option() {
  console.log(`\nStatus Covid-19 List`);
  console.log(`1. Positive`);
  console.log(`2. Negative`);
  console.log(`3. Suspect`);
  console.log(`4. Close`);

  rl.question(`Check Status : `, (Status) => {
    switch (eval(Status)) {
      case 1:
        for (let i = 0; i < covid.length; i++) {
          if (covid[i].status == "Positive") {
            console.log(`- ${covid[i].name} Positive Covid-19`);
          }
        }
        option();
        break;
      case 2:
        for (let i = 0; i < covid.length; i++) {
          if (covid[i].status == "Negative") {
            console.log(`- ${covid[i].name} Negative Covid-19`);
          }
        }
        option();
        break;
      case 3:
        for (let i = 0; i < covid.length; i++) {
          if (covid[i].status == "Suspect") {
            console.log(`- ${covid[i].name} Suspect Covid-19`);
          }
        }
        option();
        break;
      case 4:
        rl.close();
        break;
      default:
        console.log("no such option!");
        option();
    }
  });
}

option();
