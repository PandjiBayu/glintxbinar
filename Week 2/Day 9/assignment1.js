let vegetable = ["tomato", "brocolli", "kale", "cabbage", "apple"];

for (let i = 0; i < vegetable.length; i++) {
  if (vegetable[i] == "apple") {
    console.log(`${vegetable[i]} isn't vegetable!`);
  } else {
    console.log(
      `${vegetable[i]} is a healthy food, it's definitely worth to eat!`
    );
  }
}
