// Import readline
const index = require("../index"); // Import index to run rl on this file

// Function to calculate beam volume
function prism(baseArea, height) {
  return baseArea * height;
}

// Function for inputing Base Area of Prism
function inputBaseAreaPrism() {
  index.rl.question(`\nBase Area: `, (baseArea) => {
    if (!isNaN(baseArea)) {
      inputHeightPrism(baseArea); //menyimpan inputan baseArea
    } else {
      console.log(`Base Area must be a number\n`);
      inputBaseAreaPrism();
    }
  });
}

// Function for inputing Height of Prism
function inputHeightPrism(baseArea) {
  index.rl.question(`Height: `, (height) => {
    if (!isNaN(height)) {
      console.log(`\nPrism: ${prism(baseArea, height)}`);
      index.geometryMenu();
    } else {
      console.log(`Height must be a number\n`);
      inputHeightPrism(baseArea);
    }
  });
}

module.exports = { inputBaseAreaPrism }; // Export inputLength and input function, so another file can call it
