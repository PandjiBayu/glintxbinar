const EventEmitter = require("events");
const readline = require("readline");

const my = new EventEmitter();
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

my.on("Login:Failed", (email, password) => {
  console.log(`${email} and ${password} is not correct. Failed to login!`);
  rl.close();
});

my.on("Login:Success", (email) => {
  console.log(`${email} login!`);

  require("../Day 9/assignment2.js");
});

function login(email, password) {
  const passwordStoredInDatabase = "11111";

  if (password != passwordStoredInDatabase) {
    my.emit("Login:Failed", email, password);
  } else {
    my.emit("Login:Success", email);
  }
}

rl.question("Email: ", (email) => {
  rl.question("Password: ", (password) => {
    login(email, password);
  });
});
