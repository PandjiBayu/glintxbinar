const url = "https://jsonplaceholder.typicode.com";

const fetch = require("node-fetch");

// Promise
fetch(url + "/users")
  .then((response) => response.json())
  .then((data) => console.log(data))
  .catch((error) => console.log("error: " + error));

fetch(url + "/posts/1/comments")
  .then((response) => response.json())
  .then((data) => console.log(data))
  .catch((error) => console.log("error: " + error));

fetch(url + "/photos")
  .then((response) => response.json())
  .then((data) => console.log(data))
  .catch((error) => console.log("error: " + error));

fetch(url + "/albums")
  .then((response) => response.json())
  .then((data) => console.log(data))
  .catch((error) => console.log("error: " + error));

// Asycn-Await
async function tryFetch() {
  try {
    let response = await fetch(url + "/users");
    let dataResponse = await response.json();
    console.log(dataResponse);

    response = await fetch(url + "/posts/1/comments");
    dataResponse = await response.json();
    console.log(dataResponse);

    response = await fetch(url + "/photos");
    dataResponse = await response.json();
    console.log(dataResponse);

    response = await fetch(url + "/albums");
    dataResponse = await response.json();
    console.log(dataResponse);
  } catch (error) {
    throw error;
  }
}
tryFetch();
