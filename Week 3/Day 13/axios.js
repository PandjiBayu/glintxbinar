const url = "https://jsonplaceholder.typicode.com";

const axios = require("axios");

// Promise
axios
  .get(url + "/users")
  .then((data) => console.log(data.data))
  .catch((error) => console.log("error: " + error));

axios
  .get(url + "/posts/1/comments")
  .then((data) => console.log(data.data))
  .catch((error) => console.log("error: " + error));

axios
  .get(url + "/photos")
  .then((data) => console.log(data.data))
  .catch((error) => console.log("error: " + error));

axios
  .get(url + "/albums")
  .then((data) => console.log(data.data))
  .catch((error) => console.log("error: " + error));

// Asycn-Await
async function tryFetch() {
  try {
    dataResponse = await axios.get(url + "/users");
    console.log(dataResponse.data);

    dataResponse = await axios.get(url + "/posts/1/comments");
    console.log(dataResponse.data);

    dataResponse = await axios.get(url + "/photos");
    console.log(dataResponse.data);

    dataResponse = await axios.get(url + "/albums");
    console.log(dataResponse.data);
  } catch (Error) {
    throw Error;
  }
}
tryFetch();
