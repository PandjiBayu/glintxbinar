class Animal {
  constructor(name) {
    this.name = name;
  }

  static play() {
    console.log("I like to play with friends");
  }

  greet() {
    console.log("Hi everybody!");
  }
}

module.exports = Animal;
