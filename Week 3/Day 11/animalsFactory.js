// import Animal from "./animal";
const Aquatic = require("./aquatic");
const Terrestrial = require("./terrestrial");
const Amphibian = require("./amphibian");
const Animal = require("./animal");

const animal1 = new Aquatic("Fish", "an Aquatic", "Water");
const animal2 = new Terrestrial("Lion", "a Terrestrial", "Land");
const animal3 = new Amphibian("Turtle", "an Amphibian", "Water and Land");

console.log();
animal1.info();
console.log();
animal2.info();
console.log();
animal3.info();
console.log();
Animal.play();
