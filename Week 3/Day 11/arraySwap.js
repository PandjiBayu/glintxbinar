const fridge = ["Apple", "Banana", "Cucumber"];

const dataNumbers = [];
const randomNumbers = Math.floor(Math.random() * 10);

for (let index = 0; index < randomNumbers; index++) {
  dataNumbers.push(Math.floor(Math.random() * 100));
}

// sort number
dataNumbers.sort((a, b) => a - b);
console.log(dataNumbers);
// reverse number
dataNumbers.sort((a, b) => b - a);
console.log(dataNumbers);

// option 1
// let temp = fridge[0];
// fridge[0] = fridge[1];
// fridge[1] = temp;

// option 2
[fridge[0], fridge[1]] = [fridge[1], fridge[0]];

console.log(fridge);

// sort
let fridgeSort = fridge.sort();
console.log(fridgeSort);

// reverse
let fridgeReverse = fridge.reverse();
console.log(fridgeReverse);
