const Animal = require("./animal");

class Terrestrial extends Animal {
  constructor(name, species, live) {
    super(name);
    this.species = species;
    this.live = live;
  }

  info() {
    this.greet();
    console.log(`I am a ${this.name}`);
    console.log(`They usually call me ${this.species} animal, because ...`);
    console.log(`I live on ${this.live}`);
  }
}

module.exports = Terrestrial;
